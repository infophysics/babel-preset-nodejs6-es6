module.exports = {
    plugins: ['transform-export-extensions',
        'transform-async-to-generator',
        'transform-es2015-modules-commonjs'
    ]
};